import armbot.web as web

if __name__ == "__main__":
    web.app.run(host="0.0.0.0", debug=True)
