from sanic import Sanic

from armbot.web.static import bp_static

from armbot.web.websocket import bp_ws
from armbot.web.messaging import bp_msg

app = Sanic("armbot_web")
app.blueprint((bp_ws, bp_msg, bp_static))
