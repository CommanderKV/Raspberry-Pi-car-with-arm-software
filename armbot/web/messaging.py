import json
from sanic import Blueprint

bp_msg = Blueprint("messaging")

endpoints = {}


@bp_msg.websocket("/msg")
async def msg(_, socket):
    while True:
        data = await socket.recv()
        data_dict = json.loads(data)

        if data_dict["serverType"] == "register":
            endpoints[data_dict["role"]] = socket
        elif data_dict["serverType"] == "msg":
            await endpoints[data_dict["dest"]].send(data)
