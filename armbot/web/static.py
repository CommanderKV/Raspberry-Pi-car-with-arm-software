from pathlib import Path

from sanic import Blueprint

bp_static = Blueprint("static")

# Sanic doesn't fallback to index by default
bp_static.static("/", Path(__file__).parent / "static/index.html")
bp_static.static("/", Path(__file__).parent / "static")
