"use strict"

function getWSProtocol() {
    if (location.protocol == "https:") {
        return "wss:"
    } else {
        return "ws:"
    }
}

function connectWs() {
    // Connect to stick&arm control socket
    window.ws = new WebSocket(`${getWSProtocol()}//${location.host}/ws`)
    window.ws.onclose = function (e) {
        console.log("Socket closed. Reconnecting in 1s...", e.reason)
        delete window.ws
        setTimeout(function () {
            connectWs()
        }, 1000)
    }

    window.ws.onerror = function (err) {
        window.ws.close()
    }
}

connectWs()
