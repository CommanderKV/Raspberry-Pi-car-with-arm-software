let opts = {
    config: {
        iceServers: [
            {
                urls: "stun:turn.cryptty.tk"
            }
        ]
    }
}

// The server doesn't send any signal before client asks it to
// which the client does so immediately upon creating peerConnection
if (role == "client") {
    opts.initiator = true
}

// WebRTC uses a different socket
window.signalWs = new WebSocket(`${getWSProtocol()}//${location.host}/msg`)

function setupSignaling() {
    signalWs.onclose = function (e) {
        console.log("Socket closed. Reconnecting in 1s...", e.reason)
        delete window.ws
        setTimeout(function () {
            connectWs()
        }, 1000)
    }

    signalWs.onerror = function (err) {
        window.ws.close()
    }
    // Register ourself so the server knows where to send data
    const registration = {
        serverType: "register",
        role: window.role
    }
    console.log(registration)
    signalWs.send(JSON.stringify(registration))
}

function setupPeerConnection() {
    window.peerConnection = new SimplePeer(opts)

    peerConnection.on("signal", data => {
        const signalData = JSON.stringify({
            serverType: "msg",
            dest: peer,
            data: data
        })
        signalWs.send(signalData)
    })

    signalWs.onmessage = function (event) {
        const data = JSON.parse(event.data)
        peerConnection.signal(data.data)
    }

    peerConnection.on("stream", addStream)
}

function addStream(stream) {
    // Make the video show up,
    // called either locally (by addMedia())
    // or remotely (by the callback above)
    let video = document.getElementById("videoStream")

    if ('srcObject' in video) {
        video.srcObject = stream
    } else {
        video.src = window.URL.createObjectURL(stream) // for older browsers
    }
}

function addMedia() {
    navigator.mediaDevices.getUserMedia({
        video: true,
        audio: true
    }).then(function (stream) {
        peerConnection.addStream(stream)
        addStream(stream)
    })
}

signalWs.addEventListener("open", function () {
    setupSignaling()
    setupPeerConnection()
})
