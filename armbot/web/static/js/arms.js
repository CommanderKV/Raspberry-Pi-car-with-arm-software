function sendArmState() {
    let armState = {
        type: "arm",
        arms: [
            Number(document.getElementById("arm1").value),
            Number(document.getElementById("arm2").value),
        ]
    }

    window.ws.send(JSON.stringify(armState))
}

function stop() {
    let armState = {
        type: "arm",
        arms: [
            0,
            0
        ]
    }

    window.ws.send(JSON.stringify(armState))
}

function sendClawState(state) {
    let clawState = {
        type: "claw",
        claw: state
    }

    window.ws.send(JSON.stringify(clawState))
}
