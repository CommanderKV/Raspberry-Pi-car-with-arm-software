class CarState {
    type = "car"
    carX = 0
    carY = 0
}

let carState = new CarState()

// See JoyStick/joy.js
let joy = new JoyStick("joy_div", {}, function (stickData) {
    carState.carX = Number(stickData.x)
    carState.carY = Number(stickData.y)
    window.ws.send(JSON.stringify(carState))
})
