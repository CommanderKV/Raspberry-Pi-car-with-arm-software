"""
Used for the websocket connection
"""

import json
from sanic import Blueprint
import RPi.GPIO as GPIO

from armbot.controller import connection, motor, arm, claw

bp_ws = Blueprint("websocket")

GPIO.setmode(GPIO.BCM)
# in1, in2, in3, in4
motor1 = motor.Controller(17, 18, 27, 22)
_claw = claw.Claw(4, 5)
arms = arm.Arm(13, 19, 200)


@bp_ws.websocket("/ws")
async def ws(_, socket):
    """
    Call on WebSocket open

    Args:
        socket (_type_): the websocket object
    """

    while True:
        data = await socket.recv()
        data_dict = json.loads(data)

        if data_dict["type"] == "car":
            # Set the car state
            data_dict["motors"] = [motor1]
            connection.set_car_state(data_dict)

        elif data_dict["type"] == "arm":
            # Set the arm state
            arms.set_state(
                data_dict["arms"][0],
                data_dict["arms"][1],
            )

        elif data_dict["type"] == "claw":
            if data_dict["claw"] == "open":
                _claw.open()
            elif data_dict["claw"] == "close":
                _claw.close()
            elif data_dict["claw"] == "stop":
                _claw.stop()
