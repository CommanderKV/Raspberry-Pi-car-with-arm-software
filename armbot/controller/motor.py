"""
Used to control a pair of motors
"""
import RPi.GPIO as GPIO

class Controller:
    """
    Class used to control motor drivers
    """

    def __init__(self, in1: int, in2: int, in3: int, in4: int):
        """
        Used to make a motor object

        Args:
            in1 (int): The connection to pin in1 on the motor driver
            in2 (int): The connection to pin in2 on the motor driver
            in3 (int): The connection to pin in3 on the motor driver
            in4 (int): The connection to pin in4 on the motor driver
        """

        # Setup all GPIO pins
        GPIO.setup(in1, GPIO.OUT)
        GPIO.setup(in2, GPIO.OUT)
        GPIO.setup(in3, GPIO.OUT)
        GPIO.setup(in4, GPIO.OUT)

        # Start the PWM for the motor
        self.in1 = GPIO.PWM(in1, 1000)
        self.in1.start(0)
        self.in2 = GPIO.PWM(in2, 1000)
        self.in2.start(0)

        self.in3 = GPIO.PWM(in3, 1000)
        self.in3.start(0)
        self.in4 = GPIO.PWM(in4, 1000)
        self.in4.start(0)


    def move(self, motor_a_speed: int, motor_b_speed: int):
        """
        Sets the speed of the motors

        Args:
            motor_a_speed (int): Speed of the left motor
            motor_b_speed (int): Speed of the right motor
        """

        # Forward for motor A
        if motor_a_speed < 0:
            # Set the direction of the motor

            self.in1.ChangeDutyCycle(motor_a_speed * -1)
            self.in2.ChangeDutyCycle(0)


        # Backward for motor A
        elif motor_a_speed > 0:
            # Set the direction of the motor
            self.in1.ChangeDutyCycle(0)
            self.in2.ChangeDutyCycle(motor_a_speed)

        # Stop for motor A
        else:
            # Set the direction of the motor
            self.in1.ChangeDutyCycle(0)
            self.in2.ChangeDutyCycle(0)


        # Forward for motor B
        if motor_b_speed < 0:
            # Set the direction of the motor
            self.in3.ChangeDutyCycle(motor_b_speed * -1)
            self.in4.ChangeDutyCycle(0)


        # Backward for motor B
        elif motor_b_speed > 0:
            # Set the direction of the moto
            self.in3.ChangeDutyCycle(0)
            self.in4.ChangeDutyCycle(motor_b_speed)


        # Stop for motor B
        else:
            self.in3.ChangeDutyCycle(0)
            self.in4.ChangeDutyCycle(0)


    def __del__(self):
        """
        Used to clean up the GPIO pins
        """
        # Try to stop all of the
        # pin connections we dont
        # need need
        try:
            self.in1.stop()
            self.in2.stop()
            self.in3.stop()
            self.in4.stop()
            GPIO.cleanup()

        except Exception:
            print ("Error occured when cleaning up the GPIO pins")

        finally:
            print ("GPIO pins cleaned up")
