"""
Control the claw in front of the arm
"""
import RPi.GPIO as GPIO


class Claw:
    def __init__(self, in1: int, in2: int):
        GPIO.setup(in1, GPIO.OUT)
        GPIO.setup(in2, GPIO.OUT)

        self.in1 = GPIO.PWM(in1, 1000)
        self.in1.start(0)
        self.in2 = GPIO.PWM(in2, 1000)
        self.in2.start(0)

    def open(self):
        self.in1.ChangeDutyCycle(100)
        self.in2.ChangeDutyCycle(0)

    def close(self):
        self.in1.ChangeDutyCycle(0)
        self.in2.ChangeDutyCycle(100)

    def stop(self):
        for pin in (self.in1, self.in2):
            pin.ChangeDutyCycle(0)

    def __del__(self):
        for pin in (self.in1, self.in2):
            pin.stop()
