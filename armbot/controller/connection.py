"""
Used to set the cars state / control the car
"""

def set_motion(x: int, y: int, motors: list):
    """
    Set the motion of the car

    Args:
        x (int): The x position of the joystick.
        y (int): The y position of the joystick.
        motors (list): The list of motors to control.
    """


    # Set a dead zone for
    # the left right motion
    """
    if abs(x) + abs(y) < 10:
        x = 0
        y = 0
    """

    # x = 255 * (x / 100)
    # y = 255 * (y / 100)

    # Make a vairable to modify
    # the speed of the motors
    # used for turning.
    #
    # If x is positive, and y is positive: Subtract
    # If x is positive, and y is negative: Add
    # If x is negative, and y is positive: Add
    # If x is negative, and y is negative: Subtract
    #
    # Reason:
    #     -1 - -1 = 0   ->   -1 - -1 = 0
    #     -1 -  1 = -2  ->    1 + -1 = 0
    #      1 -  1 = 0   ->    1 -  1 = 0
    #      1 - -1 = 2   ->   -1 +  1 = 0
    #
    if x < 0 and y < 0:
        modifyed_speed = y - x
        turn_speed = y + x

    elif x < 0 and y > 0:
        modifyed_speed = y + x
        turn_speed = y - x

    elif x > 0 and y > 0:
        modifyed_speed = y - x
        turn_speed = y + x

    elif x > 0 and y < 0:
        modifyed_speed = y + x
        turn_speed = y - x

    else:
        modifyed_speed = 0
        turn_speed = 0

    # Make sure the modifyed speed is
    # between -100 and 100
    if modifyed_speed < -100:
        modifyed_speed = -100
    elif modifyed_speed > 100:
        modifyed_speed = 100


    # Make sure the turn_speed is
    # between -100 and 100
    if turn_speed < -100:
        turn_speed = -100
    elif turn_speed > 100:
        turn_speed = 100

    # Make sure y is within
    # -100 and 100
    if y < -100:
        y = -100
    elif y > 100:
        y = 100

    # Go over all the motors we
    # were given and set their
    # speeds respectfully
    for motor in motors:
        if x > 0:
            motor.move(turn_speed, modifyed_speed)

        elif x < 0:
            motor.move(modifyed_speed, turn_speed)

        else:
            motor.move(y, y)


def set_car_state (state: dict):
    """
    Sets the state of the car

    Args:
        details (dict): The details of the car.
    """
    # Get the X pos and
    # Y pos of the motors
    x = state['carX']
    y = state['carY']

    # Get the motors
    motors_in = state["motors"]

    # Set the motors speed
    set_motion(x, y, motors_in)
