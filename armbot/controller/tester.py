"""
Used to test states
"""

import connection

class Motor:
    """
    Class used to control motor drivers
    """

    def __init__(self):
        """
        Used to make a motor object

        Args:
            in1 (int): The connection to pin in1 on the motor driver
            in2 (int): The connection to pin in2 on the motor driver
            in3 (int): The connection to pin in3 on the motor driver
            in4 (int): The connection to pin in4 on the motor driver
            duty_cylce_pin1 (int): The connection to pin enA on the motor driver
            duty_cylce_pin2 (int): The connection to pin enB on the motor driver
        """

        self.in1 = 0
        self.in2 = 0
        self.in3 = 0
        self.in4 = 0


    def move(self, motor_a_speed: int, motor_b_speed: int):
        """
        Sets the speed of the motors

        Args:
            motor_a_speed (int): Speed of the left motor
            motor_b_speed (int): Speed of the right motor
        """

        # Forward for motor A
        if motor_a_speed < 0:
            # Set the direction of the motor

            self.in1 = motor_a_speed * -1
            self.in2 = 0

        # Backward for motor A
        elif motor_a_speed > 0:
            # Set the direction of the motor
            self.in1 = 0
            self.in2 = motor_a_speed

        # Stop for motor A
        else:
            # Set the direction of the motor
            self.in1 = 0
            self.in2 = 0


        # Forward for motor B
        if motor_b_speed < 0:
            # Set the direction of the motor
            self.in3 = motor_b_speed * -1
            self.in4 = 0

        # Backward for motor B
        elif motor_b_speed > 0:
            # Set the direction of the moto
            self.in3 = 0
            self.in4 = motor_b_speed

        # Stop for motor B
        else:
            self.in3 = 0
            self.in4 = 0


    def __del__(self):
        """
        Used to clean up the GPIO pins
        """
        # Try to stop all of the
        # pin connections we dont
        # need need
        try:
            self.in1 = 0
            self.in2 = 0
            self.in3 = 0
            self.in4 = 0

        except Exception:
            print ("Error occured when cleaning up the GPIO pins")

        finally:
            print ("GPIO pins cleaned up")


def main():
    """
    main code execution
    """

    motors = [
        Motor(),
        Motor()
    ]

    temp = open("test.txt", "w")
    temp.write("")
    temp.close()

    with open("test.txt", "a") as f:
        for x in range(-100, 101):
            for y in range(-100, 101):
                motorsCheck = []
                if motors[0].in1 == motors[1].in1:
                    motorsCheck.append(True)
                else:
                    motorsCheck.append(False)

                if motors[0].in2 == motors[1].in2:
                    motorsCheck.append(True)
                else:
                    motorsCheck.append(False)

                if motors[0].in3 == motors[1].in3:
                    motorsCheck.append(True)
                else:
                    motorsCheck.append(False)

                if motors[0].in4 == motors[1].in4:
                    motorsCheck.append(True)
                else:
                    motorsCheck.append(False)


                connection.set_car_state({"carX": x, "carY": y, "motors": motors})
                f.write(f"X: {x}\tY:{y}\t\t{motorsCheck}\n")

main()
