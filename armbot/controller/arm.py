"""
Used to set the arms state / control the arm
"""
import RPi.GPIO as GPIO


class Arm:
    """
    This class will controll all of the servos for the arm
    """

    def __init__(self, servo_A_pin: int, servo_B_pin: int, hz: int):
        """
        Initialize the arm

        Args:
            base_pin (int): The pin that the base servos connection pin is on
            servo_A_pin (int): The pin that servo A's connection pin is on
            servo_B_pin (int): The pin that servo B's connection pin is on
            servo_C_pin (int): The pin that servo C's connection pin is on
            hz (int): The frequency of the servos
        """

        self.servo_HZ = hz

        self.servos = []
        for pos, pin in enumerate([servo_A_pin, servo_B_pin]):
            # Setup all GPIO pins
            GPIO.setup(pin, GPIO.OUT)
            self.servos.append(GPIO.PWM(pin, self.servo_HZ))
            self.servos[pos].start(0)
            self.servos[pos].ChangeDutyCycle(0)

    def set_state(self, A: int, B: int):
        """
        Set the state of the arm

        Args:
            base (int): The angle of the base servo
            A (int): The angle of the servo A
            B (int): The angle of the servo B
            C (int, optional): How contracted the hand is. Defaults to 0.
        """

        # Get the required dutycycles
        # for ther servos to move to
        # the desired angle
        #
        # A formula that might look
        # more familiar to you is
        # y = 1/18*x + 2 or y = mx + b
        A_angle = A
        B_angle = B

        # Set the angle of servo A
        if 0 <= A <= 180:
            self.servos[0].ChangeDutyCycle(A_angle)
            # self.servos[0].ChangeDkutyCycle(0)

        # Set the angle of servo B
        if 0 <= B <= 180:
            self.servos[1].ChangeDutyCycle(B_angle)
            # self.servos[1].ChangeDutyCycle(kk0)

    def __del__(self):
        """
        Clean up the GPIO pins
        """
        try:
            for servo in self.servos:
                servo.stop()

            GPIO.cleanup()

        except Exception:
            print("Error occored when cleaning up the GPIO pins")

        finally:
            print("GPIO pins cleaned up")
